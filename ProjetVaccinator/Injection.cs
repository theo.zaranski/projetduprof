﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjetVaccinator
{
    class Injection
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(50)]
        public string Nom { get; set; }
        [MaxLength(50)]
        public string Prenom { get; set; }

        public bool? sexe { get; set; }

        public DateTime? DateDeNaissance { get; set; }

        public int InjectionsId { get; set; }

        public Injection injections { get; set; }
    }
}
