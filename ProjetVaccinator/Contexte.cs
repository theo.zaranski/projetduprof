﻿using Microsoft.EntityFrameworkCore;
using System;

namespace ProjetVaccinator.ORM
{
    public class Contexte : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlite("Data Source=Vaccinator.db");
        }

    }
}
