﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjetVaccinator
{
    public class Personne
    {
        public int Id { get; set; }

        [MaxLength(10)]
        public string TypeVaccin { get; set; }

        [MaxLength(20)]
        public string MarqueDuVaccin { get; set; }

        public float numLot { get; set; }

        public DateTime? Date { get; set; }

        public DateTime? DateRappel { get; set; }
    }
}
